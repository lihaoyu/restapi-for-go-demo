package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

type CoinPairInfo struct {
	ServerTime int                    `json:"server_time"`
	Pairs      map[string]interface{} `json:"pairs"`
}

//step 1: get the info from https://api.liqui.io/api/3/info and print it out
//step 2: Unmarshal the info into CoinPairInfo struct and print out the data in pair "ltc_btc"
//step 3: get the pair "ltc_btc" depth info from  https://api.liqui.io/api/3/depth and print it out
//step 4: Umarshal the data of "ltc_btc" depth into a struct and print out all the information in "asks"

//Hint for step 1: You only need copy and pasting
//Hint for step 2: You need to search what "Unmarshal" in Go is.
//Hint for step 3: You need to check the liqui api documentaion on its website https://liqui.io/api.
//Hint for step 4: Use this website https://transform.now.sh/json-to-go/ to define your own struct

func main() {
	//step 1:
	info := HttpGetRequest("")

	fmt.Println(info)

	//step 2:
	coinPairInfo := CoinPairInfo{}

	fmt.Println(coinPairInfo.Pairs[""])

	//step 3:

	//stet 4:
}


//you don't need to change anything below here

func HttpGetRequest(strUrl string) string {
	httpClient := &http.Client{}

	request, err := http.NewRequest("GET", strUrl, nil)
	if nil != err {
		return err.Error()
	}
	request.Header.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36")
	request.Header.Add("Connection", "close")

	response, err := httpClient.Do(request)
	if nil != err {
		return err.Error()
	}
	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if nil != err {
		return err.Error()
	}

	return string(body)
}
